* 2.43 (05/26/2019)
Bluetooth on by default.
Bluetooth stats everywhere.
KML export improvements.
Safe mcc mnc handling.
Adding number of satellites to dash fix.
Fixing GPS threading and LERP.
